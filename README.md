# NewsApp
News App is a simple news app which uses [NewsAPI](https://newsapi.org/) to fetch top news headlines from the API.
### The codebase focuses On:
1. Kotlin + Coroutines
2. MVVM design pattern
3. Data binding
4. FCM notification
5. Unit tests
6. Code structuring, style and comments

